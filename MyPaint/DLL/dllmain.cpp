// dllmain.cpp : Defines the entry point for the DLL application.
#include "stdafx.h"
#include <Windowsx.h>
#include <Commdlg.h>
#include <stdlib.h>
#define EXPORT  __declspec(dllexport)
HWND ghWnd;
HINSTANCE hInstance;

BOOL APIENTRY DllMain( HMODULE hModule,
                       DWORD  ul_reason_for_call,
                       LPVOID lpReserved
                     )
{
	hInstance = hModule;
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
    case DLL_PROCESS_DETACH:
        break;
    }
    return TRUE;
}

extern "C" EXPORT
void ShiftScale(POINT &pointE, POINT pointS)
{
	if (abs(pointE.y - pointS.y) < abs(pointE.x - pointS.x))
	{
		if ((pointE.x > pointS.x))
			pointE.x = pointS.x + abs(pointE.y - pointS.y);
		else
			pointE.x = pointS.x - abs(pointS.y - pointE.y);
	}
	else
	{
		if (pointE.y > pointS.y)
			pointE.y = pointS.y + abs(pointE.x - pointS.x);
		else
			pointE.y = pointS.y - abs(pointS.x - pointE.x);
	}
}

extern "C" EXPORT
BOOL OpenRead(HWND hWnd, WCHAR filename[260])
{
	OPENFILENAME ofn;
	filename[0] = '\0';
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hWnd;
	ofn.nMaxFile = 260;
	ofn.lpstrFilter = L"Paint binary (*.dat)\0*.dat\0All files (*.*)\0*.*\0";
	ofn.nFilterIndex = 2;
	ofn.nMaxFileTitle = _MAX_FNAME + _MAX_EXT;
	ofn.lpstrFile = filename;
	ofn.Flags = OFN_HIDEREADONLY | OFN_CREATEPROMPT | OFN_NOCHANGEDIR | OFN_FILEMUSTEXIST;
	return (GetOpenFileName(&ofn));
}
extern "C" EXPORT
BOOL OpenWrite(HWND hWnd, WCHAR filename[260])
{
	OPENFILENAME ofn;
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(OPENFILENAME);
	ofn.hwndOwner = hWnd;
	ofn.nMaxFile = 260;
	ofn.lpstrFilter = L"Paint binary(*.dat)\0*.dat\0All files (*.*)\0*.*\0";
	ofn.nFilterIndex = 2;
	ofn.Flags = OFN_OVERWRITEPROMPT | OFN_NOCHANGEDIR;
	ofn.lpstrFile = filename;
	ofn.lpstrDefExt = L"dat";
	return GetSaveFileName(&ofn);
}
extern "C" EXPORT
bool ChangeColor(HWND hWnd, COLORREF&curentColor)
{
	COLORREF custColors[16];
	for (int i = 0; i < 16; i++)
	{
		custColors[i] = RGB(i * 16, i * 16, i * 16);
	}
	CHOOSECOLOR cc;
	ZeroMemory(&cc, sizeof(cc));
	cc.lStructSize = sizeof(cc);
	cc.hwndOwner = hWnd;
	cc.lpCustColors = (LPDWORD)custColors;
	cc.rgbResult = curentColor;
	cc.Flags = CC_FULLOPEN | CC_RGBINIT;
	ChooseColor(&cc);
	curentColor = cc.rgbResult;
	return 1;
}

